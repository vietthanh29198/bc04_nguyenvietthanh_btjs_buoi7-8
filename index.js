// Nhap so n
var array = [];

function themSo() {
  var soN = document.getElementById("txt-nhap-so-n").value * 1;
  array.push(soN);
  document.getElementById("txt-nhap-so-n").value = "";
  document.getElementById("result").innerHTML = `${array}`;
}

// 1.Tinh tong so duong
function tinhTongSoDuong() {
  var sum = 0;
  array.forEach(function (item) {
    if (item > 0) {
      sum += item;
    }
  });
  document.getElementById("result-1").innerHTML = `Tổng số dương: ${sum}`;
}

// 2.Dem so duong
function demSoDuong() {
  var count = 0;
  array.forEach(function (item) {
    if (item > 0) {
      count++;
    }
  });
  document.getElementById("result-2").innerHTML = `có ${count} số dương`;
}

// 3.Tim so nho nhat
function timSoNhoNhat() {
  var min = array[0];
  for (index = 1; index < array.length; index++) {
    const item = array[index];
    if (item < min) {
      min = item;
    }
  }
  document.getElementById("result-3").innerHTML = `Số nhỏ nhất: ${min}`;
}

// 4.Tim so duong nho nhat
function timSoDuongNhoNhat() {
  var soDuong = array.filter(function (item) {
    return item > 0;
  });

  var minDuong = soDuong[0];
  for (index = 1; index < array.length; index++) {
    const item = soDuong[index];
    if (item < minDuong) {
      minDuong = item;
    }
  }

  document.getElementById(
    "result-4"
  ).innerHTML = `Số dương nhỏ nhất: ${minDuong}`;
}

// 5.Tim so chan cuoi cung
function timSoChanCuoiCung() {
  var soChan = array.filter(function (item) {
    return item % 2 == 0 && item > 0;
  });
  lastSoChan = soChan.pop();
  document.getElementById(
    "result-5"
  ).innerHTML = `Số chẵn cuối cùng: ${lastSoChan}`;
}

// 6.Doi cho
function doiCho() {
  var viTriSo1 = document.getElementById("txt-nhap-so-vi-tri-1").value * 1;
  var viTriSo2 = document.getElementById("txt-nhap-so-vi-tri-2").value * 1;
  var soTrungGian = array[viTriSo1];
  array[viTriSo1] = array[viTriSo2];
  array[viTriSo2] = soTrungGian;
  document.getElementById("result-6").innerHTML = `Mảng sau khi đổi: ${array}`;
}

// 7.Sap xep tang dan
function sapXep() {
  for (let i = 0; i < array.length; i++) {
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] > array[j]) {
        var temp = array[j];
        array[j] = array[i];
        array[i] = temp;
      }
    }
  }
  document.getElementById(
    "result-7"
  ).innerHTML = `Mảng sau khi sắp xếp: ${array}`;
}

// 8.Tim so nguyen to dau tien

function isPrime(n) {
  if (n < 2) return false;

  for (let index = 2; index < n - 1; index++) {
    if (n % index == 0) {
      return false;
    }
  }

  return true;
}

function timSoNguyenToDauTien() {
  var found = false;
  array.forEach(function (item) {
    if (found == false && isPrime(item)) {
      found = true;
      document.getElementById(
        "result-8"
      ).innerHTML = `Số nguyên tố đầu tiên: ${item}`;
    }
  });
}

// 9.Dem so nguyen
function demSoNguyen() {
  var count = 0;
  array.forEach(function (item) {
    if (Number.isInteger(item)) {
      count++;
    }
  });
  document.getElementById("result-9").innerHTML = `Có ${count} số nguyên`;
}

// 10.So sanh so luong so am va so duong
function soSanhSoLuongSoAmSoDuong() {
  var negativeNumberArray = array.filter(function (item) {
    return item < 0;
  });
  var positiveNumberArray = array.filter(function (item) {
    return item > 0;
  });

  countNegative = negativeNumberArray.length;
  countPositive = positiveNumberArray.length;

  if (negativeNumberArray.length > positiveNumberArray.length) {
    document.getElementById(
      "result-10"
    ).innerHTML = `số lượng số âm > số lượng số dương`;
  } else if (negativeNumberArray.length < positiveNumberArray.length) {
    document.getElementById(
      "result-10"
    ).innerHTML = `số lượng số dương > số lượng số âm`;
  } else {
    document.getElementById(
      "result-10"
    ).innerHTML = `số lượng số dương = số lượng số âm`;
  }
}
